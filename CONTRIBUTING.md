# How To Contribute 

1. **Create a branch following the git flow naming conventions (feature/*, bugfix/*, hotfix/*, etc)**
    * Please make sure to document your code and add tests if possible!

    * Select develop as the merge target by default or any applicable branch.

2. **Create a merge/pull request detailing the contents of your changes** *(Please be as descript as possible!)*

3. **Wait for approval, or apply to become a core team member**
    * We are currently a small team and looking for active contributors!
