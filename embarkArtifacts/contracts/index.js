module.exports = {
"ENSRegistry": require('./ENSRegistry.js').default,
"Resolver": require('./Resolver.js').default,
"FIFSRegistrar": require('./FIFSRegistrar.js').default,
"Congregate": require('./Congregate.js').default,
"Badge": require('./Badge.js').default,
};