pragma solidity ^0.6.0;

contract Badge {

  struct Location {
    int latitude;
    int longitude;
  }

  address public owner;
  string public userName;
  bytes32 public bio;
  uint256[] public tags;
  string public firstName;
  string public lastName;
  Location public location;

  event UpdateUsername(address indexed owner, string indexed userName);
  event UpdateName(address indexed owner, string  firstName, string lastName);
  event UpdateBio(address indexed owner, bytes32 indexed bio);
  event UpdateLocation(address indexed owner, int latitude, int longitude);
  event AddTag(address indexed owner, uint256 tag);

  constructor () public payable {
    owner = msg.sender;
    emit UpdateUsername(owner, userName);
  }

  modifier isOwner(){
      require(msg.sender == owner, 'Must be owner to update');
      _;
  }

  function updateUsername(string memory _uName) public isOwner {
    //require(msg.sender == owner, 'Must be owner to update');
    userName = _uName;
    emit UpdateUsername(owner, userName);
  }

  function updateFirstname(string memory _first) public isOwner {
    //require(msg.sender == owner, 'Must be owner to update');
    firstName = _first;
    emit UpdateName(owner, firstName, lastName);
  }

  function updateLastname(string memory _last) public isOwner {
    //require(msg.sender == owner, 'Must be owner to update');
    lastName = _last;
    emit UpdateName(owner, firstName, lastName);
  }

  function updateLatLong(int _latitude, int _longitude) public isOwner {
    //require(msg.sender == owner, 'Must be owner to update');
    location.latitude = _latitude;
    location.longitude = _longitude;
    emit UpdateLocation(owner, location.latitude, location.longitude);
  }

  function updateBio(bytes32 _bio) public isOwner {
    //require(msg.sender == owner, 'Must be owner to update');
    bio = _bio;
    emit UpdateBio(owner, bio);
  }

  function addTag(uint256 _tag) public isOwner {
    //require(msg.sender == owner, 'Must be owner to update');
    tags.push(_tag);
    emit AddTag(owner, _tag);
  }

  function getFullname() public view returns (string memory _firstName, string memory _lastName) {
    return (firstName, lastName);
  }

  function getAllInfo() public view returns (
    string memory _userName,
    string memory _firstName,
    string memory _lastName,
    int256 _latitude,
    int256 _longitude,
    bytes32 _bio,
    uint256[] memory _tags
  )
  {
      return (userName, firstName, lastName, location.latitude, location.longitude, bio, tags);
  }

}
